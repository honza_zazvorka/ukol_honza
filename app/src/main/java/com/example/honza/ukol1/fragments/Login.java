package com.example.honza.ukol1.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.honza.ukol1.R;

/**
 * Created by Honza on 25.12.2015.
 */
public class Login extends Fragment {

    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, viewGroup, false);

        return view;
    }
}
