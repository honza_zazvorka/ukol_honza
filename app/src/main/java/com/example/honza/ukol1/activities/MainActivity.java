package com.example.honza.ukol1.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.honza.ukol1.R;

public class MainActivity extends AppCompatActivity {
    EditText et1,et2;
    Button b1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        et1= (EditText)findViewById(R.id.EditText_Username);
        et2= (EditText)findViewById(R.id.EditText_Password);
        b1 = (Button)findViewById(R.id.Button_Login);




    b1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(et1.getText().toString().equals("admin") &&

                    et2.getText().toString().equals("admin")) {
                Toast.makeText(getApplicationContext(), "Příhlášní úspěšné", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
                String Username = et1.getText().toString();
                intent.putExtra("KEY", Username);

                startActivity(intent);
                finish();


            }
            else{
                Toast.makeText(getApplicationContext(), "Neexistující uživatel nebo chybné heslo!",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), GalleryActivity.class);
                String Username = et1.getText().toString();
                intent.putExtra("KEY", Username);
                startActivity(intent);
                finish();

            }
        }
    });
    }
    @Override
    public void onBackPressed() {
        try {DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout2);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        }
        catch (Exception e){
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
