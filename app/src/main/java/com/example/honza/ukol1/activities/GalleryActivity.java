package com.example.honza.ukol1.activities;



import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.honza.ukol1.R;

import com.example.honza.ukol1.adapters.ImageAdapter;
import com.example.honza.ukol1.fragments.GalleryView;
import com.example.honza.ukol1.fragments.Login;

/**
 * Created by Honza on 5.12.2015.
 */
public class GalleryActivity extends MainActivity{

    ListView listView;
    GridView gridView;

    int switcherFilter=0;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new ImageAdapter(this));
         listView = (ListView) findViewById(R.id.listView);
       listView.setAdapter(new ImageAdapter(this));*/
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout2);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        textView = (TextView) findViewById(R.id.textView5);
        Bundle extras = getIntent().getExtras();
        if(extras !=null) {
            String Username = extras.getString("KEY");

            String string = getString(R.string.username);
            textView.setText(string+ ": "+Username);
        }


/*
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Send intent to SingleViewActivity
                Intent i = new Intent(getApplicationContext(), SingleViewActivity.class);

                // Pass image index
                i.putExtra("id", position);
                startActivity(i);
            }
        });
       listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                // Send intent to SingleViewActivity
                Intent i = new Intent(getApplicationContext(), SingleViewActivity.class);

                // Pass image index
                i.putExtra("id", position);
                startActivity(i);
            }
        });*/
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final NavigationView mNavigationView = (NavigationView)findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.nav_login:
                        Fragment fragment = new Login();

                        FragmentManager manager = getSupportFragmentManager();
                        FragmentTransaction transaction = manager.beginTransaction();
                        transaction.replace(R.id.output, fragment);
                        transaction.commit();

                       Toast.makeText(getApplicationContext(), "Snaha o neco", Toast.LENGTH_SHORT).show();

                        return true;
                    case R.id.nav_gallery:
                    int switcher= 0;
                if (switcher == 0) {
                    gridView.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(), "ListView", Toast.LENGTH_SHORT).show();
                    switcher++;
                    return true;}
                if (switcher == 1) {
                    gridView.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "GridView", Toast.LENGTH_SHORT).show();
                    switcher--;
                    return true;
                }
                    case R.id.nav_filters:

                      MenuItem item0 = mNavigationView.getMenu().getItem(3);
                        MenuItem item1 = mNavigationView.getMenu().getItem(4);
                        MenuItem item2 = mNavigationView.getMenu().getItem(5);
                        MenuItem item3 = mNavigationView.getMenu().getItem(6);

                        if (switcherFilter == 0) {
                            item0.setVisible(true);
                            item1.setVisible(true);
                            item2.setVisible(true);
                            item3.setVisible(true);
                            switcherFilter++;
                            return true;}
                        else{
                            item0.setVisible(false);
                            item1.setVisible(false);
                            item2.setVisible(false);
                            item3.setVisible(false);
                            switcherFilter--;
                            return true;
                        }

                        /*Fragment fragment2 = new GalleryView();

                        FragmentManager manager2 = getSupportFragmentManager();
                        FragmentTransaction transaction2 = manager2.beginTransaction();
                        transaction2.replace(R.id.output, fragment2);
                        transaction2.commit();

                        Toast.makeText(getApplicationContext(), "Snaha o neco2", Toast.LENGTH_SHORT).show();
*/

                default:
                return true;

            }
                }
            });


    }





}